//DOM -document object model

// querySelector function takes a string input that is formatted like CSS selector when applying styles. This allows to get a specific elemets

// alternative
// document.getElementById("txt-first-name")
// document.getElementByClassName()
// document.getElementByTagName()

console.log(document)

const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')


const updateFullName =() => {

	let firstName = txtFirstName.value
	let lastName = txtLastName.value

	spanFullName.innerHTML = `${firstName} ${lastName}`	
}

	txtFirstName.addEventListener('keyup',updateFullName)

	txtLastName.addEventListener('keyup',updateFullName)
