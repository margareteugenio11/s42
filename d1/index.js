//DOM -document object model

// querySelector function takes a string input that is formatted like CSS selector when applying styles. This allows to get a specific elemets

// alternative
// document.getElementById("txt-first-name")
// document.getElementByClassName()
// document.getElementByTagName()

console.log(document)

const txtFirstName = document.querySelector('#txt-first-name')
const spanFullName = document.querySelector('#span-full-name')

	txtFirstName.addEventListener('keyup', (event) => {
		spanFullName.innerHTML = txtFirstName.value;
})

	txtFirstName.addEventListener('keyup',(e) => {
		console.log(e.target)
		console.log(e.target.value)
})


